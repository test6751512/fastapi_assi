from fastapi import FastAPI, Depends, Request
from sqlalchemy import create_engine, text, MetaData, Table
from sqlalchemy.orm import sessionmaker
from pydantic import BaseModel
from fastapi.middleware.cors import CORSMiddleware
import json

DATABASE_URL = "postgresql://postgres:Ozen4928@localhost/dvdrental"
engine = create_engine(DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)
metadata = MetaData()


app = FastAPI()


app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"],
    allow_credentials=True,)


def get_database():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


class category (BaseModel):
    category_id: int
    name: str


@app.get("/category/category_id/{value}")
def read_category(value, db=Depends(get_database)):
    with db.connection() as conn:

        coun_query = text(f"SELECT column_name FROM information_schema.columns WHERE table_name = :table_name")
        coun_result = conn.execute(coun_query, {"table_name": "category"}).fetchall()
        coun_name = [col[0] for col in coun_result]

        db_query = text(f"SELECT * FROM category WHERE category_id = :value")
        db_result = conn.execute(db_query, {"value": value}).fetchall()
        rows = [dict(zip(coun_name, row)) for row in db_result]
        return rows


@app.get("/category")
def read_category(db=Depends(get_database)):
    with db.connection() as conn:

        coun_query = text(f"SELECT column_name FROM information_schema.columns WHERE table_name = :table_name")
        coun_result = conn.execute(coun_query, {"table_name": "category"}).fetchall()
        coun_name = [col[0] for col in coun_result]

        db_query = text(f"SELECT * FROM category")
        db_result = conn.execute(db_query, {"table": "category"}).fetchall()

        rows = [dict(zip(coun_name, row)) for row in db_result]
        return rows


@app.post("/category")
async def insert_category(category: category, request: Request, db=Depends(get_database)):
    dt = await request.json()

    with db.connection() as conn:
        query = text("INSERT INTO category (category_id, name) VALUES (:category_id, :name)")
        conn.execute(query, {"category_id": dt.get("category_id"), "name": dt.get("name")})
        conn.commit()

    return {"status": "insert success"}


@app.put("/category/category_id/{value}")
async def update_category(value, category: category, request: Request, db=Depends(get_database)):
    dt = await request.json()

    with db.connection() as conn:
        query = text("UPDATE category SET category_id = :category_id, name = :name WHERE category_id = :value")
        conn.execute(query, {"value": value, "category_id": dt.get("category_id"), "name": dt.get("name")})
        conn.commit()

    return {"status": "update success"}


@app.delete("/category/category_id/{value}")
async def delete_category(value, category: category, request: Request, db=Depends(get_database)):
    dt = await request.json()

    with db.connection() as conn:
        query = text("DELETE FROM category WHERE category_id = :value ")
        conn.execute(query, {"category_id": dt.get("category_id"), "value": value})
        conn.commit()
    return {"status": "delete success"}


class Country (BaseModel):
    country_id: int
    country: str


@app.get("/country/country_id/{value}")
def read_country(value, db=Depends(get_database)):
    with db.connection() as conn:

        coun_query = text(f"SELECT column_name FROM information_schema.columns WHERE table_name = :table_name")
        coun_result = conn.execute(coun_query, {"table_name": "country"}).fetchall()
        coun_name = [col[0] for col in coun_result]

        db_query = text(f"SELECT * FROM country WHERE country_id = :value")
        db_result = conn.execute(db_query, {"value": value}).fetchall()
        rows = [dict(zip(coun_name, row)) for row in db_result]
        return rows


@app.get("/country")
def read_country(db=Depends(get_database)):
    with db.connection() as conn:

        coun_query = text(f"SELECT column_name FROM information_schema.columns WHERE table_name = :table_name")
        coun_result = conn.execute(coun_query, {"table_name": "country"}).fetchall()
        coun_name = [col[0] for col in coun_result]

        db_query = text(f"SELECT * FROM country")
        db_result = conn.execute(db_query, {"table": "country"}).fetchall()

        rows = [dict(zip(coun_name, row)) for row in db_result]
        return rows


@app.post("/country")
async def insert_country(country: Country, request: Request, db=Depends(get_database)):
    dt = await request.json()

    with db.connection() as conn:
        query = text("INSERT INTO country (country_id, country) VALUES (:country_id, :country)")
        conn.execute(query, {"country_id": dt.get("country_id"), "country": dt.get("country")})
        conn.commit()

    return {"status": "insert success"}


@app.put("/country/country_id/{value}")
async def update_country(value, country: Country, request: Request, db=Depends(get_database)):
    dt = await request.json()

    with db.connection() as conn:
        query = text("UPDATE country SET country_id = :country_id, country = :country WHERE country_id = :value")
        conn.execute(query, {"value": value, "country_id": dt.get("country_id"), "country": dt.get("country")})
        conn.commit()

    return {"status": "update success"}


@app.delete("/country/country_id/{value}")
async def delete_country(value, country: Country, request: Request, db=Depends(get_database)):
    dt = await request.json()

    with db.connection() as conn:
        query = text("DELETE FROM country WHERE country_id = :value ")
        conn.execute(query, {"country": dt.get("country"), "country_id": dt.get("country_id"), "value": value})
        conn.commit()
    return {"status": "delete success"}







